import React from 'react'
import { reduce, values } from 'lodash'
import TopBar from './top-bar'
import ExpenseDetails from './expense-details'
import ExpensesList from './expenses-list'
import { expenses } from './data'
import Form from './expense-form'

class App extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			expenses: reduce(expenses, (acc, e) => {
				acc[e.id] = e
				return acc
			}, {}),
			selected: null,
		}
	}

	flip = expense => {
		const updated = { ...expense, status: !expense.status}
		const models = { ...this.state.expenses, [updated.id]: updated}

		this.setState({
			expenses: models,
		})
	}

	select = expense => {
		this.setState({
			selected: expense.id,
		})
	}

	add = expense => {
		const { expenses } = this.state

		this.setState({
			expenses: {
				...expenses,
				[expense.id]: expense,
			}
		})
	}

	render() {
		const { expenses, selected } = this.state
		return (
			<div>
				<TopBar />

				<div className="container-fluid">
					<div className="row fill-height">
						<div className="col-md-8 pb-3">
							<ExpensesList expenses={values(expenses)} onFlip={this.flip} selected={selected} onSelect={this.select}/>
						</div>
						<div className="col-md-4" >
							{ selected && <ExpenseDetails expense={expenses[selected]}/> }
						</div>
					</div>
					<Form onAdd={this.add}/>
				</div>
			</div>
		)
	}
}

export default App
