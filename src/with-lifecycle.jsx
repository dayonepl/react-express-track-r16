import React from 'react'

const withLifecycle = Component => {
	return class extends React.Component {
		componentDidMount() {
			console.log('componentDidMount')
		}

		componentDidUpdate() {
			console.log('componentDidUpdate')
		}

		render () {
			return <Component {...this.props}/>
		}

	}
}

export default withLifecycle
