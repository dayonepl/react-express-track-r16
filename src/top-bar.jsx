import React from 'react'

class TopBar extends React.Component {
	render() {
		return <div className="top-bar">
			<h1>No more expenses</h1>
		</div>
	}
}

export default TopBar
