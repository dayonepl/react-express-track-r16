import React from 'react'

const Row = ({ e, selected, onFlip, onSelect }) => (
	<tr className={selected ? 'selected' : undefined } onClick={onSelect}>
		<td>{e.category}</td>
		<td>{e.date}</td>
		<td>{e.amount.currency + e.amount.value}</td>
		<td>{e.description}</td>
		<td onClick={(event) => {
			event.stopPropagation()

			onFlip(e)
		}}>{e.status ? 'DONE' : 'PENDING'}</td>
	</tr>
)

export default Row
