import React from 'react'
import { reduce } from 'lodash'

const Total = ({ expenses }) => (
	<tr>
		<td></td>
		<td></td>
		<td>{ reduce(expenses, (acc, e) => acc + e.amount.value, 0) }</td>
		<td></td>
	</tr>
)

export default Total
