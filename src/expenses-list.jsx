import {filter, orderBy} from 'lodash'
import React from 'react'
import Row from './expense-row'
import Total from './total'

class ExpensesList extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			column: 'category',
			filterCategory: '',
		}
	}

	changeFilter = e => {
		const v = e.target.value
		this.setState({
			filterCategory: v,
		})
	}

	sort = column => () => {
		this.setState({
			column: column,
		})
	}

	render() {
		const { expenses, selected, onFlip, onSelect } = this.props
		const { column, filterCategory } = this.state

		const ordered = orderBy(
			filter(expenses, e => e.category.startsWith(filterCategory))
			,column)

		return (
			<div className="container">
				<table className="table table-hover">
					<thead>
						<tr>
							<th onClick={this.sort('category')}>Category<input value={filterCategory} onChange={this.changeFilter}/></th>
							<th onClick={this.sort('date')}>Date</th>
							<th onClick={this.sort('amount.value')}>Amount</th>
							<th onClick={this.sort('description')}>Description</th>
							<th onClick={this.sort('status')}>Status</th>
						</tr>
					</thead>
					<tbody>
						{
							ordered.map(r =>
								<Row
									key={r.id}
									e={r}
									selected={selected === r.id}
									onSelect={() => onSelect(r)}
									onFlip={onFlip}
								/>
							)
						}
						<Total expenses={expenses}/>
					</tbody>
				</table>
			</div>
		)
	}
}

export default ExpensesList
